package programModular;

import java.util.Scanner;

/**
 *
 * @author Alba Jim�nez �lvarez
 */
public class Penjat1 {

	static Scanner reader = new Scanner(System.in);
	static String nom = " ";
	static String paraulaEscollida = " ";
	static String paraulaSecreta = " ";
	static char lletra = ' ';
	static char paraulaSecretav[] = new char[paraulaSecreta.length()];
	
	public static void main(String[] args) {

		int op = 0;
		do {
			PresentaMenu();
			op = LlegirOpcio();
			Executa(op);
		} while (op != 0);
	}

	static void PresentaMenu() {

		System.out.println("Menu d'opcions: \n");
		System.out.println("1. Mostrar Ajuda");
		System.out.println("2. Definir Jugador");
		System.out.println("3. Jugar Partida");
		System.out.println("4. Veure Jugador");
		System.out.println("0. Sortir");

	}

	static int LlegirOpcio() {
		int opcio = 0;
		boolean enter = false;
		do {
			enter = reader.hasNextInt();
			if (!enter) {
				System.out.println("Ei, ha de ser un n�mero enter.");
				reader.nextLine();
			} else {
				opcio = reader.nextInt();
				if (opcio < 0 || opcio > 4) {
					System.out.println("Ei, ha de ser un n�mero entre 0 i 4");
					enter = false;
				}
			}

		} while (!enter);
		return opcio;
	}

	static void Executa(int NumTrobat) {
		
		switch (NumTrobat) {
		case 0:
			System.out.println("Fi de programa");
			break;
		case 1:
			System.out.println("Instrucciones juego del Ahorcado:\n"
					+ "El juego consiste en adivinar una palabra secreta seleccionada por el programa de forma aleatoria.\n"
					+ "Al comienzo del juego, el programa muestra una sucesi�n de guiones. Cada gui�n contiene una letra de la palabra que se ha de adivinar.\n"
					+ "En cada tirada el jugador indica una letra. Si la letra est� dentro de la palabra, se muestra, tantas veces como aparezca, sustituyendo el gui�n correspondiente por la posici�n que ocupa.\n"
					+ "Si la letra indicada por el jugador no est� en la palabra, el jugador tendr� un fallo. Se pierde la partida, si se falla m�s de 10 veces; y se gana si se adivina completamente la palabra sin superar los 10 fallos.\n"
					+ "Mucha suerte y a jugar\n");
			break;
		case 2:

			System.out.println("Introdueix el teu nom");
			LlegirNom();
			
			Sortejar();
			for (int i = 0; i < paraulaSecreta.length(); i++) {
				paraulaSecretav[i] = '_';
			}
			for (int j = 0; j < paraulaSecretav.length; j++) {
				System.out.print(paraulaSecretav[j]+" ");
			}
			System.out.println();
			break;
		case 3:
			
			System.out.println("Introdueix la lletra que vols saber si esta:");
			LlegirLletra();
			Comprovacio();
			
		case 4:

			break;
		default:
			System.out.println("No has triat b�, torna a intentar-ho i recorda ha de ser una n�mero enter entre 0 i 4");
			break;
		}
	}

	static void LlegirNom() {
		nom = reader.nextLine();
		
	}

	static void Sortejar() {
		String paraules[] = { "rondejan", "plie", "cupe", "chase", "batmand" };
		int num = (int) (Math.random() * 5);
		paraulaSecreta = paraules[num];
		
	}
	
	static void LlegirLletra() {
		lletra = reader.next().charAt(0);
	}
	
	static boolean Comprovacio() {
		boolean esta = false;
		for(int i = 0; i<paraulaSecreta.length(); i++) {
			if(lletra==paraulaSecreta.charAt(i)) {
				paraulaSecretav[i] = lletra;
				esta = true;
			}else {
				esta = false;
			}
			
		}
		return esta;
	}
}
