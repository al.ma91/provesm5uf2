package programModular;

import java.util.Scanner;

/**
 *<h2>Clase Calculadora2, s'utiliza per calcular sumes, restes, multiplicacions i divisions. </h2>
 *
 * @version 1-2021
 * @author Alba Jim�nez �lvarez
 * @since 16-02-2021
 */
public class Calculadora2 {

	/**
	 * Atribut Operand 1
	 */
	public static int N1 = 0;
	/**
	 * Atribut Operand 1
	 */
	public static int N2 = 0;
	/**
	 * Atribut Resultat
	 */
	public static int result = 0;
	
	/**
	 * Comprova que el n�mero s'ha introdu�t
	 * @return <ul>
	 *           <li>true: el nombre s'ha introdu�t</li>
	 *           <li>false: el nombre no s'ha introdu�t</li>
	 *         </ul>
	 */
	public static boolean A = false;
	
	public static boolean operacio = false;

	
	public static void main(String[] args) {

		char Op = ' ';
		while (Op != 'g') {
			PresentaMenu();
			Op = DemanaLletra();
			Executa(Op);
		}
	}

	/**
	 * Presenta el menu*/
	public static void PresentaMenu() {

		System.out.println("Menu d'opcions: \n");
		System.out.println("a. Obtenir dos n�meros a operar");
		System.out.println("b. Sumar");
		System.out.println("c. Restar");
		System.out.println("d. Multiplicar");
		System.out.println("e. Divisi�");
		System.out.println("f. Visualitzar el resultat de l�operaci�.");
		System.out.println("g. Sortir del programa");

	}

	/**
	 * Demana lletra i filtra que estigui dins les possibilitats
	 * @return char
	 */
	public static char DemanaLletra() {
		char LletraTrobada;
		Scanner reader = new Scanner(System.in);
		LletraTrobada = reader.next().charAt(0);
		return LletraTrobada;
	}

	/**
	 * Llegeix el numero introdu�t i filtra que estigui dins les possibilitats
	 * @return int
	 */
	public static int LlegirOperand() {
		int operand = 0;
		boolean enter = false;
		Scanner reader = new Scanner(System.in);
		do {
			enter = reader.hasNextInt();
			if (!enter) {
				System.out.println("Ei, ha de ser un n�mero enter.");
				reader.nextLine();
			}

		} while (!enter);
		operand = reader.nextInt();

		return operand;
	}
	
	/**
	 * Executa les opcions del men�
	 * 
	 */
	public static void Executa(char Caracter) {
		Scanner reader = new Scanner(System.in);
		char div = ' ';

		switch (Caracter) {
		case 'a':
			System.out.println("Introdueix un n�mero ");
			N1 = LlegirOperand();
			System.out.println("Introdueix un n�mero ");
			N2 = LlegirOperand();
			A = true;
			break;
		case 'b':
			if(A){
				result = suma();
				operacio = true;
			}else {
				System.out.println("Ei no has introdu�t cap n�mero");
			}
			break;
		case 'c':
			if(A){
				result = resta();
				operacio = true;
			}else {
				System.out.println("Ei no has introdu�t cap n�mero");
			}
			break;
		case 'd':
			if(A){
				result = multi();
				operacio = true;
			}else {
				System.out.println("Ei no has introdu�t cap n�mero");
			}
			break;
		case 'e':
			if(A){
				System.out.println("Vols el quocient (q) o el residu (r) de la operaci�?");
				do {
					div = reader.nextLine().charAt(0);
					if (div == 'q') {
						result = divQ();
						operacio = true;
					} else if (div == 'r') {
						result = divR();
						operacio = true;
					} else {
						System.out.println("Ei ha de ser q o r");
					}
				} while (div != 'q' && div != 'r');
			}else {
				System.out.println("Ei no has introdu�t cap n�mero");
			}
			break;
		case 'f':
			if(operacio&&A) {
				System.out.println("El resultat de la �ltima operaci� �s " + result);
			}else {
				System.out.println("Ei no hi ha cap operaci� realitzada!");
			}
			
			break;
		case 'g':
			System.out.println("Fi de programa");
			break;
		default:
			System.out.println("No has triat b�, torna a intentar-ho i recorda ha de ser una lletra de la a fins la f");
			break;
		}
		reader.close();
	}

	/**
	 * Calcula la suma de dos operands
	 * @return int
	 */
	public static int suma() {
		int suma;
		suma = N1 + N2;
		return suma;
	}

	/**
	 * Calcula la resta de dos operands
	 * @return int
	 */
	public static int resta() {
		int resta;
		resta = N1 - N2;
		return resta;
	}
	
	/**
	 * Calcula la multiplicacio de dos operands
	 * @return int
	 */
	public static int multi() {
		int multi;
		multi = N1 * N2;
		return multi;
	}

	/**
	 * Calcula la divisio sense decimals de dos operands
	 * @return int
	 */
	public static int divQ() {
		int divQ;
		divQ = N1 / N2;
		return divQ;
	}

	/**
	 * Calcula la modularitat de dos operands
	 * @return int
	 */
	public static int divR() {
		int divR;
		divR = N1 % N2;
		return divR;
	}
	
}
